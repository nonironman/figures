﻿using Figures.Library;

/// <summary> Тесты окружности </summary>
public class CircleTests : FigureTestsBase<Circle>
{
    public CircleTests() => Figure = new(2);

    protected override Circle Figure { get; }

    protected override double GetArea() => 12.56;

    protected override Circle TryCreateInvalidFigure() => new(-1);
}