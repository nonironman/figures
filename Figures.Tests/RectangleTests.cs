﻿using Figures.Library;

/// <summary> Тесты прямоугольника </summary>
public class RectangleTests : FigureTestsBase<Rectangle>
{
    public RectangleTests() => Figure = new(5, 6);

    protected override Rectangle Figure { get; }

    protected override double GetArea() => 30;

    protected override Rectangle TryCreateInvalidFigure() => new(-1, 3);
}