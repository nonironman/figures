﻿using Figures.Library;
using Xunit;
using FluentAssertions;

/// <summary> Тесты треугольника </summary>
public class TriangleTests : FigureTestsBase<Triangle>
{
    public TriangleTests() => Figure = new(3, 4, 5);

    protected override Triangle Figure { get; }

    protected override double GetArea() => 6;

    protected override Triangle TryCreateInvalidFigure() => new(2, 3, 6);

    /// <summary> Тест проверки прямоугольности треугольника </summary>
    [Fact]
    public void RightAngleTest()
    {
        var testFigure = new Triangle(2, 3, 5);

        Figure.IsRightAngled().Should().BeTrue();
        testFigure.IsRightAngled().Should().BeFalse();
    }
}