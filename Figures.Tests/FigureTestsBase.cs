﻿using System;
using FluentAssertions;
using Xunit;
using Figures.Library;

/// <summary>
/// Базовые тесты фигур
/// </summary>
/// <typeparam name="TFigure"> Тип фигуры </typeparam>
public abstract class FigureTestsBase<TFigure>
    where TFigure : Figure
{
    private const double Tolerance = 0.01;

    protected abstract TFigure Figure { get; }

    /// <summary> Тест подсчета площади </summary>
    [Fact]
    public void CalculateAreaTest()
    {
        var area = Figure.GetArea();

        var assert = GetArea();

        area.Should().BeApproximately(assert, Tolerance);
    }

    /// <summary> Тест срабатывания исключения при инициализации невалидной фигуры </summary>
    [Fact]
    public void InvalidThrowsExceptionTest()
    {
        Func<TFigure> act = TryCreateInvalidFigure;
        act.Should().Throw<Exception>();
    }

    protected abstract double GetArea();

    protected abstract TFigure TryCreateInvalidFigure();
}