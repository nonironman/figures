﻿using System;
using Figures.Library;

var circle = new Circle(4);
var rectangle = new Rectangle(2, 4);
var triangle = new Triangle(3, 4, 5);

Console.WriteLine(circle.GetArea());
Console.WriteLine(rectangle.GetArea());

var isRightAngled = triangle.IsRightAngled() ? " Квадратный" : string.Empty;
Console.WriteLine(triangle.GetArea() + isRightAngled);