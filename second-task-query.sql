SELECT p.Name, c.Name
FROM Products p
    LEFT OUTER JOIN ProductCategories pc
    ON pc.ProductId = p.Id
        JOIN Categories c
        ON pc.CategoryId = c.Id