﻿namespace Figures.Library
{
    using System;

    /// <summary> Фигура </summary>
    public abstract record Figure
    {
        protected Figure()
        {
            if (!IsValid())
                throw new Exception("Ошибка создания фигуры. Проверьте задаваемые стороны");
        }

        /// <summary> Возвращает площадь </summary>
        public abstract double GetArea();

        /// <summary> Проверяет фигуру </summary>
        protected abstract bool IsValid();
    }
}