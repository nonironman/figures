﻿namespace Figures.Library
{
    using System;
    using System.Linq;

    /// <summary>
    /// Треугольник
    /// </summary>
    /// <param name="SideA"> Сторона A </param>
    /// <param name="SideB"> Сторона B </param>
    /// <param name="SideC"> Сторона C </param>
    public record Triangle(double SideA, double SideB, double SideC) : Figure
    {
        /// <inheritdoc />
        public override double GetArea()
        {
            var p = (SideA + SideB + SideC) / 2;

            return Math.Sqrt(p * (p - SideA) * (p - SideB) * (p - SideC));
        }
        
        /// <summary> Проверяет прямоугольность треугольника </summary>
        public bool IsRightAngled()
        {
            var sides = new[] { SideA, SideB, SideC }
                .OrderBy(x => x)
                .ToList();

            var leastSide = sides[0];
            var middleSide = sides[1];
            var biggestSide = sides[2];

            var result = Math.Pow(biggestSide, 2) - (Math.Pow(middleSide, 2) + Math.Pow(leastSide, 2));

            return Math.Abs(result) < 0.01;
        }

        protected override bool IsValid() =>
            SideA > 0 &&
            SideB > 0 &&
            SideC > 0 &&
            SideA + SideB >= SideC &&
            SideB + SideC >= SideA &&
            SideA + SideC >= SideB;
    }
}