﻿namespace Figures.Library
{
    /// <summary>
    /// Прямоугольник
    /// </summary>
    /// <param name="SideA"> Сторона A </param>
    /// <param name="SideB"> Сторона B </param>
    public record Rectangle(double SideA, double SideB) : Figure
    {
        /// <inheritdoc />
        public override double GetArea() => SideA * SideB;

        protected override bool IsValid() => SideA > 0 && SideB > 0;
    }
}