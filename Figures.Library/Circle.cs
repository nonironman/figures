﻿namespace Figures.Library
{
    using System;

    /// <summary>
    /// Окружность
    /// </summary>
    /// <param name="Radius"> Радиус </param>
    public record Circle(double Radius) : Figure
    {
        /// <inheritdoc />
        public override double GetArea() => Math.PI * Math.Pow(Radius, 2);

        protected override bool IsValid() => Radius > 0;
    }
}